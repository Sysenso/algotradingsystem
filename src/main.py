import json
from config import getUserConfig, getServerConfig, getSystemConfig
from flask import Flask, render_template, request, redirect, url_for
from zerodha import loginZerodha, getKite, setThread, getThread
from algo import startAlgo, movingAverageAlgo, WilliamsAlgo, getAlgoOrders
import logging
import threading
import time
from instruments import fetchInstruments, getInstrumentDataBySymbol, get_all_instruments
from datetime import datetime
import numpy as np

app = Flask(__name__)
app.config['DEBUG'] = True
threads = list()


def init_logging_config():
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO, datefmt="%Y-%m-%d %H:%M:%S")


@app.route('/', methods=['GET', 'POST'])
def home():
    if 'loggedIn' in request.args and request.args['loggedIn'] == 'true':
        kite = getKite()
        instrumentData = get_all_instruments(kite)
        return render_template('base.html')
    elif 'algoStarted' in request.args and request.args['algoStarted'] == 'true':
        return render_template('index_algostarted.html')
    else:
        return render_template('index.html')


@app.route('/chart/<chart_id>', methods=['GET'])
def show_chart(chart_id):
    return render_template('popout_chart.html', chart_id=chart_id)


@app.route('/algo', methods=['GET'])
@app.route('/news', methods=['GET'])
@app.route('/charts', methods=['GET'])
def make_redirect():
    return redirect(url_for('home'))


@app.route('/apis/broker/login/zerodha', methods=['GET'])
def login_broker():
    return loginZerodha(request.args)


@app.route('/datas/data.json', methods=['GET'])
def get_sample_data():
    with open('datas/data.json', 'r') as user:
        jsonUserData = json.load(user)
        return jsonUserData


@app.route('/sandbox/candle.json', methods=['GET'])
def get_stock_data():
    with open('../sandbox/candleStick.json', 'r') as user:
        jsonUserData = json.load(user)
        return jsonUserData


# @app.route('/sandbox/extra.json', methods=['GET'])
# def get_extra_data():
#     with open('../sandbox/extra.json', 'r') as user:
#         jsonUserData = json.load(user)
#         return jsonUserData



@app.route('/algo_start')
# TODO: Thread management for every algorithm
def start_algo():
    setThread(True)
    thread1 = threading.Thread(target=startAlgo)
    threads.append(thread1)
    thread2 = threading.Thread(target=WilliamsAlgo)
    threads.append(thread2)
    thread1.start()
    thread2.start()
    systemConfig = getSystemConfig()
    homeUrl = systemConfig['homeUrl'] + '?algoStarted=true'
    logging.info('Sending redirect url %s in response', homeUrl)
    respData = {'redirect': homeUrl}
    return json.dumps(respData)


@app.route('/algo_stop')
def stop_algo():
    global stopThreads
    setThread(False)
    for index, thread in enumerate(threads):
        thread.join()
    logging.info('Stoping the Algo')
    systemConfig = getSystemConfig()
    homeUrl = systemConfig['homeUrl'] + '?loggedIn=true'
    logging.info('Sending redirect url %s in response', homeUrl)
    respData = {'redirect': homeUrl}
    return json.dumps(respData)


@app.route('/positions', methods=['GET'])
def positions():
    kite = getKite()
    positions = kite.positions()

    # the json file where the output must be stored
    out_file = open("../sandbox/positions.json", "w")

    json.dump(positions, out_file, indent=4)

    out_file.close()

    # print('getKite positions => ', positions)
    return json.dumps(positions)


@app.route('/orders', methods=['GET'])
def orders():
    kite = getKite()
    orders = kite.orders()

    for i in range(len(orders)):
        if orders[i]["order_timestamp"] is not None:
            orders[i]["order_timestamp"] = orders[i]["order_timestamp"].isoformat()
        if orders[i]["exchange_timestamp"] is not None:
            orders[i]["exchange_timestamp"] = orders[i]["exchange_timestamp"].isoformat()

    final_orders = {"data": orders}

    # the json file where the output must be stored
    out_file = open("../sandbox/orders.json", "w")

    json.dump(final_orders, out_file, indent=4)

    out_file.close()

    # print('getKite orders => ', orders)
    return json.dumps(orders)


@app.route('/news', methods=['GET'])
def news():
    return render_template('news.html')


@app.route('/save_as_csv', methods=['GET'])
def saveToCSV():
    # from the numpy module
    np.savetxt("datas/algo_orders.csv",
               getAlgoOrders(),
               delimiter=", ",
               fmt='% s')
    return


@app.route('/algo_orders', methods=['GET'])
def algoOrders():
    algoOrders = getAlgoOrders()
    return json.dumps(algoOrders)


@app.route('/get_instrument', methods=['GET'])
def getInstrument():
    kite = getKite()
    instrumentData = get_all_instruments(kite)
    return instrumentData


@app.route('/get_historical/<transaction>')
def getHistorical(transaction):
    kite = getKite()
    instrumentData = kite.instruments(exchange='NSE')

    params = transaction.split("&")

    fromDate = params[1]
    toDate = params[2]
    interval = params[3]

    candle_data = []

    for value in instrumentData:
        if value["tradingsymbol"] == params[0]:
            instrumentToken = value["instrument_token"]
            records = kite.historical_data(instrumentToken, from_date=fromDate, to_date=toDate, interval=interval)
            for val in records:
                temp_list = []
                temp_list.append(val["date"].isoformat())
                temp_list.append(val["open"])
                temp_list.append(val["high"])
                temp_list.append(val["low"])
                temp_list.append(val["close"])
                candle_data.append(temp_list)
    final_candle = {"candles": candle_data}

    # the json file where the output must be stored
    out_file = open("../sandbox/candle.json", "w")

    json.dump(final_candle, out_file, indent=4)

    out_file.close()

    with open('../sandbox/candle.json', 'r') as user:
        jsonUserData = json.load(user)
        return jsonUserData


# Execution starts here
init_logging_config()

serverConfig = getServerConfig()
logging.info('serverConfig => %s', serverConfig)

userConfig = getUserConfig()
logging.info('userConfig => %s', userConfig)

port = serverConfig['port']

app.run('localhost', port)
