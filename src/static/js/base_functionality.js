
// initially display only algorithm
$(document).ready(function(){
    $(".algo-order-wrapper").hide();
    $(".holding-wrapper").hide();
    $(".orders-wrapper").hide();
    $(".news-wrapper").hide();
    $(".chart-wrapper").hide();
    $(".nav-algo").addClass("active");
//    history.replaceState(null, '', 'algo');
});

// show content on click
$(".nav-algo").click(function() {
    removeActive();
    $(".nav-algo").addClass("active");
    hideContent();
    $(".algo-wrapper").show()
    $(".master-control").show()
//    history.replaceState(null, '', 'algo');
})

// show content on click
$(".nav-algo-order").click(function() {
    removeActive();
    $(".nav-algo-order").addClass("active");
    hideContent();
    $(".algo-order-wrapper").show()
    $(".master-control").show()
//    history.replaceState(null, '', 'algo-order');
})

$(".nav-position").click(function(e) {
    // calling a python function
    e.preventDefault()
    loadPositions()
    removeActive();
    $(".nav-position").addClass("active");
    hideContent();
    $(".holding-wrapper").show()
    $(".master-control").hide()
    // history.replaceState(null, '', 'holdings');
})

$(".nav-order").click(function(e) {
    // calling a python function
    e.preventDefault()
    loadOrders()
    removeActive();
    $(".nav-order").addClass("active");
    hideContent();
    $(".orders-wrapper").show()
    $(".master-control").hide()
//    history.replaceState(null, '', 'orders');
})

$(".nav-news").click(function() {
    removeActive();
    $(".nav-news").addClass("active");
    hideContent();
    $(".news-wrapper").show()
    $(".master-control").hide()
//    history.replaceState(null, '', 'news');
})

$(".nav-chart").click(function() {
    removeActive();
    $(".nav-chart").addClass("active");
    hideContent();
    $(".chart-wrapper").show()
    $(".master-control").hide()
//    history.replaceState(null, '', 'charts');
})      

function removeActive() {
    let navItems = $(".nav-items a");
    for (let i = 0; i < navItems.length; i++){
        navItems.eq(i).removeClass("active");
    }         
}

function hideContent() {
    let mainContent = $(".main-content");
    for (let i = 0; i < mainContent.length; i++){
        mainContent.eq(i).hide();
    }
    
}

// master control
$( ".master-control .on-button" ).click(function() {
    $(".algorithm").removeClass("react-off react-abort react-sq")
    $(".algorithm").addClass("react-on")
});
$( ".master-control .turn-off" ).click(function() {
    $(".algorithm").removeClass("react-on react-abort react-sq")
    $(".algorithm").addClass("react-off")
});
$( ".master-control .abort" ).click(function() {
    $(".algorithm").removeClass("react-on react-off react-sq")
    $(".algorithm").addClass("react-abort")
});
$( ".master-control .sq" ).click(function() {
    $(".algorithm").removeClass("react-on react-off react-abort")
    $(".algorithm").addClass("react-sq")
});

//   control for individual algorithms
$( ".algorithm .on-button" ).click(function() {
    $(".algorithm").eq($(this).val()-1).removeClass("react-off react-abort react-sq")
    $(".algorithm").eq($(this).val()-1).addClass("react-on")
});
$( ".algorithm .turn-off" ).click(function() {
    $(".algorithm").eq($(this).val()-1).removeClass("react-on react-abort react-sq")
    $(".algorithm").eq($(this).val()-1).addClass("react-off")
});
$( ".algorithm .abort" ).click(function() {
    $(".algorithm").eq($(this).val()-1).removeClass("react-on react-off react-sq")
    $(".algorithm").eq($(this).val()-1).addClass("react-abort")
});
$( ".algorithm .sq" ).click(function() {
    $(".algorithm").eq($(this).val()-1).removeClass("react-on react-off react-abort")
    $(".algorithm").eq($(this).val()-1).addClass("react-sq")
});
$(".logout").click(function() {
var confirmLogout = confirm("Are you sure you want to logout?");
    if (confirmLogout)
        window.location.href =  "http://localhost:8090";
})

$('.master-control .on-button').on('click', function(e) {
e.preventDefault()
$.getJSON('/algo_start',
    function(data) {
  //do nothing
})
return false;
})

$('.master-control .off .off-button').on('click', function(e) {
e.preventDefault()
$.getJSON('/algo_stop',
    function(data) {
  //do nothing
})
return false;
})

$('.csv-btn').on('click', function(e) {
e.preventDefault()
$.getJSON('/save_as_csv',
    function(data) {
  //do nothing
})
return false;
})




//window.onbeforeunload = function() {
//return "Data will be lost if you leave the page, are you sure?";
//};

