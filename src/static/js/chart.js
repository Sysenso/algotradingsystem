function autocomplete(inp, arr) {
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  /*execute a function when someone writes in the text field:*/
  inp.addEventListener("input", function (e) {
    var a, b, i, val = this.value;
    /*close any already open lists of autocompleted values*/
    closeAllLists();
    if (!val) { return false; }
    currentFocus = -1;
    /*create a DIV element that will contain the items (values):*/
    a = document.createElement("DIV");
    a.setAttribute("id", this.id + "autocomplete-list");
    a.setAttribute("class", "autocomplete-items");
    /*append the DIV element as a child of the autocomplete container:*/
    this.parentNode.appendChild(a);
    /*for each item in the array...*/
    for (i = 0; i < arr.length; i++) {
      /*check if the item starts with the same letters as the text field value:*/
      if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
        /*create a DIV element for each matching element:*/
        b = document.createElement("DIV");
        /*make the matching letters bold:*/
        b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
        b.innerHTML += arr[i].substr(val.length);
        /*insert a input field that will hold the current array item's value:*/
        b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
        /*execute a function when someone clicks on the item value (DIV element):*/
        b.addEventListener("click", function (e) {
          /*insert the value for the autocomplete text field:*/
          inp.value = this.getElementsByTagName("input")[0].value;
          /*close the list of autocompleted values,
          (or any other open lists of autocompleted values:*/
          closeAllLists();
        });
        a.appendChild(b);
      }
    }
  });
  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function (e) {
    var x = document.getElementById(this.id + "autocomplete-list");
    if (x) x = x.getElementsByTagName("div");
    if (e.keyCode == 40) {
      /*If the arrow DOWN key is pressed,
      increase the currentFocus variable:*/
      currentFocus++;
      /*and and make the current item more visible:*/
      addActive(x);
    } else if (e.keyCode == 38) { //up
      /*If the arrow UP key is pressed,
      decrease the currentFocus variable:*/
      currentFocus--;
      /*and and make the current item more visible:*/
      addActive(x);
    } else if (e.keyCode == 13) {
      /*If the ENTER key is pressed, prevent the form from being submitted,*/
      e.preventDefault();
      if (currentFocus > -1) {
        /*and simulate a click on the "active" item:*/
        if (x) x[currentFocus].click();
      }
    }
  });
  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  document.addEventListener("click", function (e) {
    closeAllLists(e.target);
  });
}
fetch('/get_instrument').then(function (response) {
  return response.json()
}).then(function (obj) {
  var instrumentData = obj
  var tradingSymbol = []

  for (let i = 0; i < instrumentData.length; i++) {
    tradingSymbol.push(instrumentData[i]["tradingsymbol"])
  }

  autocomplete(document.getElementById("myInput"), tradingSymbol);

})

var data;


$("#show-chart").click(function () {
  generateChart("graph-plot", $("#myInput")[0].value, $("#from-date")[0].value, $("#to-date")[0].value, $("#interval")[0].value)
})

function loadCandle(symbol, fromDate, toDate, interval) {
 let url = "get_historical/" + symbol + "&" + fromDate + "&" + toDate + "&" + interval
 fetch(url).then(function (response) {
    return response.json()
  }).then(function (obj) {
      data = obj
  })

}


function generateChart(currentDiv, symbol, fromDate, toDate, interval) {
let url = "get_historical/" + symbol + "&" + fromDate + "&" + toDate + "&" + interval
  fetch(url).then(function (response) {
    return response.json()
  }).then(function (obj) {

     data = obj;

     let updated = false

    // total number of datas
    let dataLength = data.candles.length;

    let initialX = []
    let initialOpen = []
    let initialHigh = []
    let initialLow = []
    let initialClose = []

    for (let i = 0; i < data.candles.length; i++) {
      initialX.push(data.candles[i][0].substring(0, 10) + ' ' + data.candles[i][0].substring(11, 16))
      initialOpen.push(data.candles[i][1])
      initialHigh.push(data.candles[i][2])
      initialLow.push(data.candles[i][3])
      initialClose.push(data.candles[i][4])
    }

    // data for initial plot
    var trace1 = {

      x: [...initialX],

      close: [...initialClose],

      decreasing: { line: { color: '#e63946' } },

      high: [...initialHigh],

      increasing: { line: { color: '#29bf12' } },

      line: { color: 'rgba(31,119,180,1)' },

      low: [...initialLow],

      open: [...initialOpen],

      type: 'candlestick',
      xaxis: 'x',
      yaxis: 'y'
    };

    let index = dataLength;

    function generateCandleData(index) {
      updated = false

      if(index < data.candles.length){
            candleInfo = data.candles[index];

      trace1.x.push(candleInfo[0].substring(0, 10) + ' ' + candleInfo[0].substring(11, 16));
      trace1.open.push(candleInfo[1]);
      trace1.high.push(candleInfo[2]);
      trace1.low.push(candleInfo[3]);
      trace1.close.push(candleInfo[4]);

      layout = {
        xaxis: {
          rangeslider: { range: [data.candles[0][0].substring(0, 10) + ' ' + data.candles[0][0].substring(11, 16), candleInfo[0].substring(0, 10) + ' ' + candleInfo[0].substring(11, 16)] }
        }
      }
      updated = true
      }
    }

    var layout = {
      title: {
        text: "",
        x: 0
      },
      dragmode: 'zoom',
      margin: {
        r: 10,
        t: 25,
        b: 40,
        l: 60
      },
      showlegend: false,
      xaxis: {
        autorange: true,
        domain: [0, 1],
        rangeslider: { range: [data.candles[0][0].substring(0, 10) + ' ' + data.candles[0][0].substring(11, 16), data.candles[data.candles.length - 1][0].substring(0, 10) + ' ' + data.candles[data.candles.length - 1][0].substring(11, 16)] },
        title: 'Date',
        type: 'date'
      },
      yaxis: {
        autorange: true,
        domain: [0, 1],
        type: 'linear'
      }
    };

    Plotly.newPlot(currentDiv, [trace1], layout)


    let intervalAcces = setInterval(function () {

      dataLength = data.candles.length

      if (index >= dataLength) {
        loadCandle(symbol, fromDate, toDate, interval)
      }

      generateCandleData(index);

      if(updated){
      var extendTrace = {
        open: [[trace1.open[trace1.open.length -1]]],
        high: [[trace1.high[trace1.high.length -1]]],
        low: [[trace1.low[trace1.low.length -1]]],
        close: [[trace1.close[trace1.close.length -1]]],
        x: [[trace1.x[trace1.x.length -1]]]
      }
          index += 1
          Plotly.extendTraces(currentDiv, extendTrace, [0]);
      }

    }, 10000);
  })

}