let instrumentsAvailable = ["PNB", "UNION BANK", "Macy’s", "Facebook", "Nike", "Apple", "Amazon", "Walmart", "Netflix", "Starbucks"];
let instrument = $(".instrument-names")[0];
let addedInstruments = [];
let currentChartDisplayed = 0;
for (let i = 0; i < instrumentsAvailable.length; i++) {
    var row = `<div class="ins-ch-wrap">
                <input type="checkbox"  class="regular-checkbox big-checkbox" id=${i + 1}>
                <h2 class="instrument-title">${instrumentsAvailable[i]}</h2>
                </div>`
    instrument.innerHTML += row
}
$('input[type=checkbox]').change(
    function () {
        if (this.checked) {
            if (addedInstruments.includes(this.id)) {
                // show content
                let currentGraph = "#graph-" + this.id;
                $(currentGraph).show();
                currentChartDisplayed += 1;
                if (currentChartDisplayed === 0) {
                    $(".no-chart-info").show();
                }
                else {
                    $(".no-chart-info").hide();
                }
            }
            else {
                addChart(this.id);
            }

        }
        else {
            hideChart(this.id);
            currentChartDisplayed -= 1;
            if (currentChartDisplayed === 0) {
                $(".no-chart-info").show();
            }
            else {
                $(".no-chart-info").hide();
            }
        }
    });

function addChart(chartId) {
    var graph_div = document.createElement("div");
    graph_div.classList.add("highlight-bottom")
    graph_div.id = "graph-" + chartId;
    $(".chart")[0].appendChild(graph_div);

    //graph to clone
    var popOutDiv = document.createElement("div");
    // graph_div.classList.add("highlight-bottom")
    popOutDiv.id = "graph-" + instrumentsAvailable[parseInt(chartId) - 1];
    $(".for-pop-out")[0].appendChild(popOutDiv);

    generateChart(graph_div.id, popOutDiv.id, chartId); 

    // for popup
    gallery="static/icon/pop-out32x32.png";
    let instrumentId = instrumentsAvailable[chartId-1]
    $("#graph-" + chartId).append("<div class='popout'><img class='roll' id='" + instrumentId + "' src='" + gallery + "'></div>");

    addedInstruments.push(chartId);
    document.getElementById(instrumentsAvailable[parseInt(chartId) - 1]).onclick = function() {
        popOut(this.id);
    }
    currentChartDisplayed += 1;
    if (currentChartDisplayed === 0) {
        $(".no-chart-info").show();
    }
    else {
        $(".no-chart-info").hide();
    }
}


function hideChart(chartId) {
    let currentGraph = "#graph-" + chartId
    $(currentGraph).hide()
    // var removeElement = document.getElementById("graph-" + chartId);
    // removeElement.style.display = "none"
}

function popOut(currentChartId){
    var html = $('.for-pop-out').html();
    window.open(html, "__new", "width=1000,height=1000");
}

function generateChart(currentDiv, currentPopOutDiv, instrumentIndex) {
    fetch('../../../sandbox/candleStick.json').then(function (response) {
        return response.json()
    }).then(function (obj) {

        var data = obj;

        // total number of datas
        let dataLength = data.candles.length;

        // data for initial plot
        var trace1 = {

            x: ["2015-12-28 09:15"],

            close: [1385.1],

            decreasing: { line: { color: '#e63946' } },

            high: [1388],

            increasing: { line: { color: '#29bf12' } },

            line: { color: 'rgba(31,119,180,1)' },

            low: [1381.05],

            open: [1386.4],

            type: 'candlestick',
            xaxis: 'x',
            yaxis: 'y'
        };

        let index = 1;

        function generateCandleData(index) {

            candleInfo = data.candles[index];

            trace1.x.push(candleInfo[0].substring(0, 10) + ' ' + candleInfo[0].substring(11, 16));
            trace1.open.push(candleInfo[1]);
            trace1.high.push(candleInfo[2]);
            trace1.low.push(candleInfo[3]);
            trace1.close.push(candleInfo[4]);

            layout = {
                xaxis: {
                    range: ['2015-12-28 09:15', candleInfo[0].substring(0, 10) + ' ' + candleInfo[0].substring(11, 16)],
                    rangeslider: { range: ['2015-12-28 09:15', candleInfo[0].substring(0, 10) + ' ' + candleInfo[0].substring(11, 16)] }
                }
            }

        }

        var layout = {
            title: {
                text: instrumentsAvailable[instrumentIndex - 1],
                x: 0
            },
            dragmode: 'zoom',
            margin: {
                r: 10,
                t: 25,
                b: 40,
                l: 60
            },
            showlegend: false,
            xaxis: {
                autorange: true,
                domain: [0, 1],
                range: ['2015-12-28 09:15', '2015-12-28 09:32'],
                rangeslider: { range: ['2015-12-28 09:15', '2015-12-28 09:32'] },
                title: 'Date',
                type: 'date'
            },
            yaxis: {
                autorange: true,
                domain: [0, 1],
                range: [1381, 1500],
                type: 'linear'
            }
        };

        Plotly.newPlot(currentDiv, [trace1], layout)
        Plotly.newPlot(currentPopOutDiv, [trace1], layout)


        let intervalAcces = setInterval(function () {

            generateCandleData(index);

            var extendTrace = {
                open: [[trace1.open[index]]],
                high: [[trace1.high[index]]],
                low: [[trace1.low[index]]],
                close: [[trace1.close[index]]],
                x: [[trace1.x[index]]]
            }
            index += 1;
            Plotly.extendTraces(currentDiv, extendTrace, [0]);
            Plotly.extendTraces(currentPopOutDiv, extendTrace, [0]);


            if (index >= dataLength) {
                clearInterval(intervalAcces);
            }


        }, 10000);
    })

}

