function loadPositions() {
  fetch('/positions').then(function(response) {
    return response.json()
  }).then(function(obj) {
      buildNetTable(obj.net)
      buildDayTable(obj.day)
  })

}

// display selected rows
function buildNetTable(sampleData) {
  // displaying net history
  document.getElementById('net-position').innerHTML = `Positions (${sampleData.length})`
  let data = sampleData
  var table = document.getElementById('net-positionTable')
  table.innerHTML = ''
  for (var i = 0; i < sampleData.length; i++) {
    var row = `<tr>
                            <td>${data[i].product}</td>
                            <td>${data[i].tradingsymbol}</td>
                            <td>${data[i].quantity}</td>
                            <td>${data[i].average_price}</td>
                            <td>${data[i].last_price}</td>
                            <td>${data[i].pnl.toFixed(2)}</td>
                        </tr>`
    table.innerHTML += row

  }
}

// display selected rows
function buildDayTable(sampleData) {
  // displaying day's history
  document.getElementById('day-position').innerHTML = `Day's History (${sampleData.length})`
  let data = sampleData
  var table = document.getElementById('day-positionTable')
  table.innerHTML = ''
  for (var i = 0; i < sampleData.length; i++) {
    var row = `<tr>
                            <td>${data[i].product}</td>
                            <td>${data[i].tradingsymbol}</td>
                            <td>${data[i].quantity}</td>
                            <td>${data[i].average_price}</td>
                            <td>${data[i].last_price}</td>
                            <td>${data[i].pnl.toFixed(2)}</td>
                        </tr>`
    table.innerHTML += row

  }
}

