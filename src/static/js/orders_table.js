function loadOrders() {
  fetch('/orders').then(function(response) {
    return response.json()
  }).then(function(obj) {
      buildOrdersTable(obj)
  })
}


// display selected rows
function buildOrdersTable(sampleData) {
document.getElementById('total-orders').innerHTML = `Executed Orders (${sampleData.length})`
  var table = document.getElementById('myTable')
  table.innerHTML = ''
  for (var i = 0; i < sampleData.length; i++) {
      var row = `<tr>
                      <td>${sampleData[i].order_timestamp.substr(11, 8)}</td>
                      <td>${sampleData[i].transaction_type}</td>
                      <td>${sampleData[i].tradingsymbol}</td>
                      <td>${sampleData[i].product}</td>
                      <td>${sampleData[i].quantity}</td>
                      <td>${sampleData[i].average_price}</td>
                      <td>${sampleData[i].status}</td>
                  </tr>`
      table.innerHTML += row

  }
}