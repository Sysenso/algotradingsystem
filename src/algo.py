import logging
from zerodha import getKite, setThread, getThread
from instruments import fetchInstruments
from quotes import getCMP
from orders import placeOrder, modifyOrder, placeSLOrder, cancelOrder
from utils import roundToNSEPrice
from ticker import startTicker, stopTicker, registerSymbols
import time
import pandas as pd
import talib
from datetime import date, datetime, timedelta
import calendar
import json
import requests
from numpy import mean
import math

algoOrders = []
PEWilliamsFlag = None
CEWilliamsFlag = None


def getAlgoOrders():
    global algoOrders
    return algoOrders


def roundup(base_index, multiple):
    lower = int(math.floor(base_index / float(multiple))) * multiple
    upper = int(math.ceil(base_index / float(multiple))) * multiple

    return lower, upper


def getWeeklyExpiryDate():
    day_value = {"Monday": 1, "Tuesday": 2, "Wednesday": 3, "Thursday": 4, "Friday": 5, "Saturday": 6, "Sunday": 7}
    dayname = datetime.today().strftime('%A')
    day_diff = 4 - day_value[dayname]
    if day_diff < 0:
        next_thursday = datetime.now() + timedelta(days=7 + day_diff)
    else:
        next_thursday = datetime.now() + timedelta(days=day_diff)
    # find if next thursday is last in month
    last_day = calendar.monthrange(next_thursday.year, next_thursday.month)[1]
    if next_thursday.day + 7 > last_day:
        month_name = datetime.today().strftime('%b')
        year = datetime.today().strftime('%y')
        final_string = year + month_name.upper()
        return final_string, True, None
    else:
        year = next_thursday.strftime('%y')
        month = next_thursday.strftime('%m').lstrip('0')
        date_no = next_thursday.strftime('%d')
        final_string = year + month + date_no
        return final_string, False, next_thursday


def startAlgo():
    count = 1
    while True:
        time.sleep(10)
        if not getThread():
            logging.info(' Start Algo killed...')
            break
        count += 1
        # print(count)


def WilliamsAlgo():
    kite = getKite()
    date_str = getWeeklyExpiryDate()
    fromDate = datetime.strftime(datetime.now(), "%Y-%m-%d")
    toDate = datetime.strftime(datetime.now(), "%Y-%m-%d")
    # Intervals
    interval = "minute"
    # Instrument tokens
    # [tradingsymbol,"rangevalue",quantity]
    tokens = [["NIFTY 50", 100, 200]]

    # TODO: ceInstrument, peInstrument == -1

    global PEWilliamsFlag
    global CEWilliamsFlag

    prevCEInstrument = None
    prevPEInstrument = None

    while True:
        time.sleep(10)
        if not getThread():
            logging.info(' Williams Algo killed...')
            break

        ceInstrument, peInstrument, callIndexSymbol, putIndexSymbol = getOptionsLabel(tokens[0][0], tokens[0][1])

        # If Transaction Label Changed
        if prevPEInstrument != peInstrument:
            if prevPEInstrument is None or PEWilliamsFlag == "Sell":
                prevPEInstrument = peInstrument
            else:
                peInstrument = prevPEInstrument

        if prevCEInstrument != ceInstrument:
            if prevCEInstrument is None or CEWilliamsFlag == "Sell":
                prevCEInstrument = ceInstrument
            else:
                ceInstrument = prevCEInstrument

        # Fetch the historical data at the end of 1min
        # PE Instrument
        records = kite.historical_data(peInstrument, from_date=fromDate, to_date=toDate, interval=interval)
        df = pd.DataFrame(records)
        if df.empty:
            continue
        openPrice = df["open"].values
        highPrice = df["high"].values
        lowPrice = df["low"].values
        closePrice = df["close"].values
        volume = df["volume"].values

        williamsPercent = talib.WILLR(highPrice, lowPrice, closePrice, timeperiod=20)

        type_flag = PEWilliamsFlag

        for val in williamsPercent[::-1]:
            if -20 <= val <= 0:
                type_flag = "Buy"
                break
            elif -80 >= val >= -100:
                type_flag = "Sell"
                break

        if type_flag != PEWilliamsFlag:
            if PEWilliamsFlag is None:
                if type_flag == "Buy":
                    # TODO: insert algo order
                    algoOrdersRecord = (str(datetime.now()), putIndexSymbol, type_flag, "Williams", closePrice[-1])
                    algoOrders.append(algoOrdersRecord)
                    PEWilliamsFlag = type_flag
            else:
                # TODO: insert algo order
                # check the position if available then sell it, else skip it
                algoOrdersRecord = (str(datetime.now()), putIndexSymbol, type_flag, "Williams", closePrice[-1])
                algoOrders.append(algoOrdersRecord)
                PEWilliamsFlag = type_flag

        # CE Instrument
        records = kite.historical_data(ceInstrument, from_date=fromDate, to_date=toDate, interval=interval)
        df = pd.DataFrame(records)
        if df.empty:
            continue
        openPrice = df["open"].values
        highPrice = df["high"].values
        lowPrice = df["low"].values
        closePrice = df["close"].values
        volume = df["volume"].values

        williamsPercent = talib.WILLR(highPrice, lowPrice, closePrice, timeperiod=20)

        type_flag = CEWilliamsFlag

        for val in williamsPercent[::-1]:
            if -20 <= val <= 0:
                type_flag = "Buy"
                break
            elif -80 >= val >= -100:
                type_flag = "Sell"
                break

        if type_flag != CEWilliamsFlag:
            if CEWilliamsFlag is None:
                if type_flag == "Buy":
                    # TODO: insert algo order
                    algoOrdersRecord = (str(datetime.now()), callIndexSymbol, type_flag, "Williams", closePrice[-1])
                    algoOrders.append(algoOrdersRecord)
                    CEWilliamsFlag = type_flag
            else:
                # TODO: insert algo order
                algoOrdersRecord = (str(datetime.now()), callIndexSymbol, type_flag, "Williams", closePrice[-1])
                algoOrders.append(algoOrdersRecord)
                CEWilliamsFlag = type_flag
        print(algoOrders)


def getOptionsLabel(token, diffValue):
    kite = getKite()

    dateStr = getWeeklyExpiryDate()

    fromDate = datetime.strftime(datetime.now(), "%Y-%m-%d")
    toDate = datetime.strftime(datetime.now(), "%Y-%m-%d")
    # Intervals
    interval = "minute"

    instrumentToken = None

    with open('../sandbox/instruments.json', 'r') as ins:
        instruments = json.load(ins)
        for currentIns in instruments:
            if currentIns["tradingsymbol"] == token:
                instrumentToken = currentIns["instrument_token"]

    records = kite.historical_data(instrumentToken, from_date=fromDate, to_date=toDate, interval=interval)

    # TODO: exception handling

    currentValue = records[-1]["close"]

    lower, upper = roundup(currentValue, diffValue)

    dateStr, lastThursday, workingDay = getWeeklyExpiryDate()

    putIndex = dateStr + str(upper) + "PE"
    callIndex = dateStr + str(lower) + "CE"

    putQuote = kite.quote(['NFO:NIFTY' + putIndex])
    # Date settings for put Index
    limit = 3
    while len(putQuote) == 0 and limit > 0:
        workingDay = workingDay - timedelta(days=1)
        year = workingDay.strftime('%y')
        month = workingDay.strftime('%m').lstrip('0')
        date_no = workingDay.strftime('%d')
        putIndex = year + month + date_no + str(upper) + "PE"
        putQuote = kite.quote(['NFO:NIFTY' + putIndex])
        limit -= 1

    if limit <= 0:
        peInstrument = -1
    else:
        peInstrument = putQuote['NFO:NIFTY' + putIndex]['instrument_token']

    # Date settings for call Index

    if limit <= 0:
        ceInstrument = -1
    elif limit == 3:
        callQuote = kite.quote(['NFO:NIFTY' + callIndex])
        ceInstrument = callQuote['NFO:NIFTY' + callIndex]['instrument_token']
    else:
        year = workingDay.strftime('%y')
        month = workingDay.strftime('%m').lstrip('0')
        date_no = workingDay.strftime('%d')
        callIndex = year + month + date_no + str(lower) + "CE"
        callQuote = kite.quote(['NFO:NIFTY' + callIndex])
        ceInstrument = callQuote['NFO:NIFTY' + callIndex]['instrument_token']

    return ceInstrument, peInstrument, 'NIFTY' + callIndex, 'NIFTY' + putIndex


def movingAverageAlgo():
    # logging.info("Moving Average started...")
    kite = getKite()
    # Date
    fromDate = datetime.strftime(datetime.now() - timedelta(2), "%Y-%m-%d")
    toDate = datetime.today().strftime("%Y-%m-%d")
    # Intervals
    interval = "minute"
    # Instrument tokens
    # [tradingsymbol,"Stock",quantity,LTPAdjustment,squareoff,stoploss,trailing_stoploss,buyFlag,sellFlag,initSmaDiff,maxSmaDiff]
    tokens = [[12680706, "BANKNIFTY21JANFUT", 25, 5, 100, 30, 1, False, False, None, None],
              [12762370, "NIFTY21JANFUT", 75, 2, 50, 20, 1, False, False, None, None]]
    while True:
        # logging.info('Moving Average running...')
        time.sleep(1)
        if not getThread():
            logging.info('Moving Average killed...')
            break

        # Fetch the historical data at the end of 1min
        if (datetime.now().second == 0) and (datetime.now().minute % 1 == 0):
            for ind in range(len(tokens)):
                records = kite.historical_data(tokens[ind][0], from_date=fromDate, to_date=toDate, interval=interval)
                df = pd.DataFrame(records)
                if df.empty:
                    continue
                df.drop(df.tail(1).index, inplace=True)
                openPrice = df["open"].values
                highPrice = df["high"].values
                lowPrice = df["low"].values
                closePrice = df["close"].values
                volume = df["volume"].values

                sma5 = talib.SMA(closePrice, 5)
                sma20 = talib.SMA(closePrice, 20)
                print(sma5)
                print(sma20)

                price = kite.ltp("NFO:" + tokens[ind][1])
                ltp = price["NFO:" + tokens[ind][1]]["last_price"]

                # sma5 crossover sma20
                if (sma5[-2] < sma20[-2]) and (sma5[-1] > sma20[-1]):
                    smaDiff = sma5[-1] - sma20[-1]
                    if not tokens[ind][7]:
                        buyOrderID = kite.place_order(variety=kite.VARIETY_BO,
                                                      exchange=kite.EXCHANGE_NFO,
                                                      tradingsymbol=tokens[ind][1],
                                                      transaction_type=kite.TRANSACTION_TYPE_BUY,
                                                      quantity=tokens[ind][2],
                                                      product=kite.PRODUCT_MIS,
                                                      order_type=kite.ORDER_TYPE_SL,
                                                      price=ltp - tokens[ind][3],
                                                      squareoff=tokens[ind][4],
                                                      stoploss=tokens[ind][5],
                                                      trailing_stoploss=tokens[ind][6],
                                                      validity=kite.VALIDITY_DAY)
                        logging.info('Buying initiated %s...', buyOrderID)
                        tokens[ind][7] = True
                        tokens[ind][9] = smaDiff
                        tokens[ind][10] = tokens[ind][9]
                    elif smaDiff > tokens[ind][9]:
                        tokens[ind][9] = smaDiff
                        tokens[ind][10] = smaDiff
                        continue
                    elif smaDiff < tokens[ind][10] / 4:
                        continue
                    elif (sma5[-2] > sma20[-2]) and (sma5[-1] < sma20[-1]):
                        tokens[ind][7] = False
                        cancelOrderID = kite.cancel_order(
                            variety=kite.VARIETY_REGULAR,
                            order_id=buyOrderID)
                        continue
                    else:
                        orderStatus = kite.order_history(order_id=buyOrderID)
                        if orderStatus.data["status"] == kite.STATUS_CANCELLED:
                            tokens[ind][7] = False

                # sma5 crossunder sma20
                if tokens[ind][8] or ((sma5[-2] > sma20[-2]) and (sma5[-1] < sma20[-1])):
                    smaDiff = sma20[-1] - sma5[-1]
                    if not tokens[ind][8]:
                        sellOrderID = kite.place_order(variety=kite.VARIETY_BO,
                                                       exchange=kite.EXCHANGE_NFO,
                                                       tradingsymbol=tokens[ind][1],
                                                       transaction_type=kite.TRANSACTION_TYPE_SELL,
                                                       quantity=tokens[ind][2],
                                                       product=kite.PRODUCT_MIS,
                                                       order_type=kite.ORDER_TYPE_SL,
                                                       price=ltp - tokens[ind][3],
                                                       squareoff=tokens[ind][4],
                                                       stoploss=tokens[ind][5],
                                                       trailing_stoploss=tokens[ind][6],
                                                       validity=kite.VALIDITY_DAY)
                        logging.info('Selling initiated %s...', sellOrderID)
                        tokens[ind][8] = True
                        tokens[ind][9] = smaDiff
                        tokens[ind][10] = tokens[ind][9]
                    elif smaDiff > tokens[ind][9]:
                        tokens[ind][9] = smaDiff
                        tokens[ind][10] = smaDiff
                        continue
                    elif smaDiff < tokens[ind][10] / 4:
                        continue
                    elif (sma5[-2] < sma20[-2]) and (sma5[-1] > sma20[-1]):
                        tokens[ind][8] = False
                        cancelOrderID = kite.cancel_order(
                            variety=kite.VARIETY_REGULAR,
                            order_id=sellOrderID)
                        continue
                    else:
                        orderStatus = kite.order_history(order_id=sellOrderID)
                        if orderStatus.data["status"] == kite.STATUS_CANCELLED:
                            tokens[ind][8] = False
