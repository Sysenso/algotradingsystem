import json
import logging
import threading
import time

instrumentsList = None
symbolToInstrumentMap = None
tokenToInstrumentMap = None


def fetchInstruments(kite):
    global instrumentsList
    if instrumentsList:
        return instrumentsList

    logging.info('Going to fetch instruments...')
    instrumentsList = kite.instruments(exchange='NSE')
    global symbolToInstrumentMap
    global tokenToInstrumentMap
    symbolToInstrumentMap = {}
    tokenToInstrumentMap = {}
    for isd in instrumentsList:
        tradingSymbol = isd['tradingsymbol']
        instrumentToken = isd['instrument_token']
        # logging.info('%s = %d', tradingSymbol, instrumentToken)
        symbolToInstrumentMap[tradingSymbol] = isd
        tokenToInstrumentMap[instrumentToken] = isd

    logging.info('Fetching instruments done. Instruments count = %d', len(instrumentsList))
    return instrumentsList


def getInstrumentDataBySymbol(tradingSymbol):
    return symbolToInstrumentMap[tradingSymbol]


def getInstrumentDataByToken(instrumentToken):
    return tokenToInstrumentMap[instrumentToken]


def get_all_instruments(kite):
    instruments = kite.instruments(exchange='NSE')

    # the json file where the output must be stored
    out_file = open("../sandbox/instruments.json", "w")

    json.dump(instruments, out_file, indent=4, default=str)

    out_file.close()

    # print('getKite instruments => ', instruments)
    return json.dumps(instruments)
